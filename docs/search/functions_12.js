var searchData=
[
  ['uncopyable_0',['uncopyable',['../structruntime_1_1uncopyable.html#aa416767c85f534764b7af109bb8296da',1,'runtime::uncopyable::uncopyable()=default'],['../structruntime_1_1uncopyable.html#a51f3981474a295e51b97e3ec5c0d944f',1,'runtime::uncopyable::uncopyable(const uncopyable &amp;)=delete']]],
  ['unmovable_1',['unmovable',['../structruntime_1_1unmovable.html#aa96e22c8060bc326ba970cdb21e01134',1,'runtime::unmovable::unmovable()=default'],['../structruntime_1_1unmovable.html#a4a27bc9aad61bd2705f484a78b486b82',1,'runtime::unmovable::unmovable(unmovable &amp;&amp;)=delete']]],
  ['upload_2',['upload',['../namespaceruntime_1_1network.html#a35f46d10040fb2d1802fbacf1cdcd659',1,'runtime::network::upload(std::string_view url, std::string_view field, std::string_view content_type, std::string_view filename, data_flow output_needed)'],['../namespaceruntime_1_1network.html#a3e23681f8cb3206195a68fe7034e96c3',1,'runtime::network::upload(std::string_view url, std::string_view field, std::string_view content_type, const std::vector&lt; uint8_t &gt; &amp;buffer, data_flow output_needed)']]],
  ['upload_5ferror_3',['upload_error',['../classvk_1_1error_1_1upload__error.html#a3dd17c35e1b4fb357ad5359a9d0b9bf6',1,'vk::error::upload_error']]],
  ['user_5fid_4',['user_id',['../classvk_1_1oauth_1_1client.html#a890622463c6e4f4841a5f485ef06ce96',1,'vk::oauth::client']]],
  ['user_5ftoken_5',['user_token',['../namespacevk_1_1config.html#ad6fb8bd558b51957905addb165df785d',1,'vk::config']]],
  ['username_6',['username',['../namespacevk_1_1config.html#ae6c8db1ae1fbfb43e6386f0eddf51fc5',1,'vk::config']]],
  ['utf8_5fcreate_7',['utf8_create',['../namespaceruntime_1_1string__utils_1_1implementation.html#ab0c629e0cd59a8f0c1d3d7116aa6cec6',1,'runtime::string_utils::implementation']]],
  ['utf8_5fto_5flower_8',['utf8_to_lower',['../namespaceruntime_1_1string__utils.html#ac96035eb276b71e3f74f510fa022b2eb',1,'runtime::string_utils']]],
  ['utf8_5fto_5fupper_9',['utf8_to_upper',['../namespaceruntime_1_1string__utils.html#aa96cab605c27663de19a89428dd32ce1',1,'runtime::string_utils']]]
];
