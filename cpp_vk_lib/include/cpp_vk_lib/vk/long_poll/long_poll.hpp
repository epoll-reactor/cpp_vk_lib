#ifndef VK_LONG_POLL_API_HPP
#define VK_LONG_POLL_API_HPP

#include "asio/io_context.hpp"
#include "asio/post.hpp"
#include "cpp_vk_lib/runtime/misc/cppdefs.hpp"
#include "cpp_vk_lib/runtime/uncopyable.hpp"
#include "cpp_vk_lib/runtime/unmovable.hpp"
#include "cpp_vk_lib/vk/events/common.hpp"
#include "cpp_vk_lib/vk/events/type.hpp"

#include <unordered_map>
#include <vector>

namespace simdjson::dom {
class parser;
}// namespace simdjson::dom

namespace vk {
/*! Event queue for groups long poll. */
class long_poll : public runtime::uncopyable, public runtime::unmovable
{
    /*! groups.getLongPollServer wrapper. */
    struct poll_payload
    {
        std::string key;
        std::string server;
        std::string ts;
    };

public:
    /*!
     * This initially will retrieve ID of group (which owns the token).
     *
     * \throws exception::access_error if group_id wasn't received.
     */
    long_poll(asio::io_context&);
    /*! Destructor to satisfy correct opaque pointer handling. */
    ~long_poll();
    /*!
     * Setup action on selected event type.
     *
     * \note If invalid event type provided, nothing will happen.
     */
    void on_event(event::type type, std::function<void(const vk::event::common&)>&& executor);
    /*!
     * Start infinite event loop. Before that, you should setup all your event handlers
     * with long_poll::on_event. Once all tasks are completed, the queue starts to
     * wait for another events.
     */
    void run(int8_t timeout = 60);

private:
    /*! Get new long poll server. */
    void server();
    /*!
     * Do listen and collect incoming events.
     * At first start, new long poll server retrieved.
     *
     * \note
     *        - If long poll returned error with code 2 or 3, new server data is requested.
     *        - This function called recursively if VK returned error. However, this case is
     *          not regular, so we don't worry about depth of this recursion.
     *
     * \return Events list.
     */
    std::vector<event::common> listen(int8_t timeout = 60);

    /*! Reference to event asynchronous task queue. */
    asio::io_context& io_context_;
    /*! Received from polling information. */
    poll_payload poll_payload_;
    /*! Tracked group id. */
    int64_t group_id_;
    /*! Mapping of executors for each specified event type (new message, wall reply, etc). */
    std::unordered_map<event::type, std::function<void(const vk::event::common&)>> executors_;
    /*! Shared parser according to the simdjson authors precepts. */
    std::unique_ptr<simdjson::dom::parser> shared_parser_;
};

}// namespace vk

#endif// VK_LONG_POLL_API_HPP
